package com.example.patterns

// Класс презентации
class Presentation(val name: String)

//Класс массива с презентациями
class Presentations(val novellas: MutableList<Presentation> = mutableListOf()) : Iterable<Presentation> {
    override fun iterator(): Iterator<Presentation> = PresentationIterator(novellas)
}

//Класс итератора
class PresentationIterator(val presentations: MutableList<Presentation> = mutableListOf(), var current: Int = 0) : Iterator<Presentation> {
    override fun hasNext(): Boolean = presentations.size > current
    override fun next(): Presentation {
        val presentation= presentations[current]
        current++
        return presentation
    }
}

fun main(args: Array<String>) {
    val novellas = Presentations(mutableListOf(Presentation("Test1"), Presentation("Test2")))
    novellas.forEach { println(it.name) }
}
package com.example.patterns

interface CoffeeMachine{
    fun makeSmallCoffee()
    fun makeLargeCoffee()
}

class NormalCoffeeMachine: CoffeeMachine{
    override fun makeSmallCoffee() = print("Normal: small coffee");
    override fun makeLargeCoffee() = print("Normal: large coffee");
}

class EnchantedCoffeeMachine(private val coffeeMachine: CoffeeMachine): CoffeeMachine by coffeeMachine{

    override fun makeLargeCoffee() {
        println("Enchanted: large coffee");
    }

    fun makeCoffeeWithMilk(){
        println("Enchanted: making coffe with milk");
        coffeeMachine.makeSmallCoffee()
        addMilk();
    }

    private fun addMilk(){
        println("Enchanted: adding milk")
    }
}


fun main(){
    val normalMachine = NormalCoffeeMachine()
    val enchantedMachine = EnchantedCoffeeMachine(normalMachine);

    enchantedMachine.makeSmallCoffee();
    enchantedMachine.makeLargeCoffee();
    enchantedMachine.makeCoffeeWithMilk();
}

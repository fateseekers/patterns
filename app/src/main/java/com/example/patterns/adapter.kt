package com.example.patterns

interface Pixel {
    var width: Double;
}

class PixelWidth(override var width: Double): Pixel

class PointsWidth(private val pixelWidth: PixelWidth) : Pixel {

    override var width: Double
    get() = convertPixelsToPoint(pixelWidth.width);
    set(width){
        pixelWidth.width = convertPointsToPixels(width)
    }

    private fun convertPointsToPixels(f: Double): Double = f/0.75

    private fun convertPixelsToPoint(f: Double): Double = 0.75*f

}

fun main (){
    val pixels = PixelWidth(320.0);
    val points = PointsWidth(pixels);

    println("Pixels: " + pixels.width);
    println("Points: " +points.width)
}
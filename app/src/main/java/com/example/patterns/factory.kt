package com.example.patterns

class FactoryParser {
    companion object {
        fun createFromFileName(fileName: String, structure: Structure) {
            val ext: String = fileName.substringAfterLast('.')
            println(ext == structure.show())
        }
    }
}

interface Structure {
    fun show(): String
}

class Xml : Structure {
    override fun show() = "xml"
}

class Json : Structure {
    override fun show() = "json"
}

fun main(args: Array<String>) {
    FactoryParser.createFromFileName("pack.xml", Json())
    FactoryParser.createFromFileName("pack.xml", Xml())
}
package com.example.patterns

class CPU {
    fun freeze() = println("Freezing.")

    fun jump(position: Long) = println("Jump to $position.")

    fun execute() = println("Executing.")
}

class HardDrive {
    fun read() = println("Reading...")
}

/* Facade */
class Computer(private val processor: CPU = CPU(), private val hd: HardDrive = HardDrive()) {
    fun start() {
        processor.freeze()
        processor.jump(0)
        processor.execute();
        hd.read()
    }
}

fun main(args: Array<String>) {
    val computer = Computer()
    computer.start()
}
package com.example.patterns

data class Memento(val state: String)

class Originator(var state: String) {

    fun createMemento(): Memento {
        return Memento(state)
    }

    fun restore(memento: Memento) {
        state = memento.state
    }
}

class CareTaker {
    private val mementoList = ArrayList<Memento>()

    fun saveState(state: Memento) {
        mementoList.add(state)
    }

    fun restore(index: Int): Memento {
        return mementoList[index]
    }
}

fun main() {
    val careTaker = CareTaker()
    val originator = Originator("initial state")
    careTaker.saveState(originator.createMemento())

    originator.state = "State #1"
    originator.state = "State #2"
    careTaker.saveState(originator.createMemento())

    originator.state = "State #3"
    println("Current State: " + originator.state)

    originator.state = "State #4"
    println("Current State: " + originator.state)

    originator.state = "State #5"
    println("Current State: " + originator.state)

    originator.state = "State #6"
    println("Current State: " + originator.state)

    originator.state = "State #7"
    println("Current State: " + originator.state)

    originator.state = "State #8"
    println("Current State: " + originator.state)

    originator.restore(careTaker.restore(1))
    println("Second saved state: " + originator.state)

    originator.restore(careTaker.restore(0))
    println("First saved state: " + originator.state)
}
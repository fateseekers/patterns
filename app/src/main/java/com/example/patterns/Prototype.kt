package com.example.patterns

//Создаем модель касса велосипеда, который будем клонировать
open class Bike : Cloneable {

    private var gears: Int = 0
    private var bikeType: String? = null

    var model: String? = null
        private set

    //Создаем первой объект класса
    init {
        bikeType = "Standard"
        model = "Leopard"
        gears = 4
    }

    //??
    public override fun clone(): Bike {
        return Bike()
    }

    //Образец нового велосипеда
    fun makeAdvanced() {
        bikeType = "Advanced"
        model = "Jaguar"
        gears = 6
    }
}

//Создаем образец клона класса Bike используя новые данные и возвращаем его
fun makeJaguar(basicBike: Bike): Bike {
    basicBike.makeAdvanced()
    return basicBike
}


fun main(args: Array<String>) {
    //Первоначальный объект
    val bike = Bike()

    //Образец для создания клона
    val basicBike = bike.clone()

    //Клон основанный на образце
    val advancedBike = makeJaguar(basicBike)

    //выводим
    println("First class object: " + bike.model)
    println("Modified class object: " + advancedBike.model)
}
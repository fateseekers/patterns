package com.example.patterns

abstract class Car(){

    fun start(){
        println("Машина завелась!")
    }

    private fun move(){
        println("Машина тронулась!");
    }

    fun stop(){
        println("Машина остановилась!");
    }

    open fun moving(){
        this.start();
        this.move();
        this.stop()
    }
}

class NormalCar: Car()


class BoatCar: Car() {

    private fun swim(){
        println("Машина плывет!");
    }

    override fun moving(){
        start();
        swim();
        stop();
    }
}

class FlyCar: Car(){

    private fun fly(){
        println("Машина взлетает");
    }

    override fun moving(){
        start()
        fly()
        stop()
    }
}

fun main(){

    val simpleCar = NormalCar()
    val boatCar = BoatCar()
    val flyCar = FlyCar()

    println("Simple car");
    simpleCar.moving();
    println("Boat car");
    boatCar.moving();
    println("Fly car");
    flyCar.moving();
}
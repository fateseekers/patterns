package com.example.patterns

import java.util.*

//класс билдер
open class ComputerBuilder(private val config: Builder){

    //Функции на вывод информацииж
    fun info():String {
        return config.os + " " + config.ram + " " + config.battery
    }

    companion object Builder {
        private var os: String? = null
        private var ram: Int? = 0
        private var battery: String? = null

        //Функции установки и получения параметров
        fun os(value: String)= apply { os = value }
        fun ram(value: Int) = apply { ram = value }
        fun battery(value: String) = apply { battery = value }

        fun build(): ComputerBuilder{
            return ComputerBuilder(this)
        }
    }
}

fun main(){
    val PC = ComputerBuilder.Builder
        .os("Windows")
        .ram(2)
        .battery("Li-On")
        .build()

    print(PC.info())
}

